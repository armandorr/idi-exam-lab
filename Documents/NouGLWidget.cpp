#include "NouGLWidget.h"

#include <iostream>

NouGLWidget::NouGLWidget(QWidget *parent) : MyGLWidget(parent) {
  grabKeyboard();
}

NouGLWidget::~NouGLWidget ()
{
}

void NouGLWidget::paintGL() {
  MyGLWidget::paintGL();
  
  glBindVertexArray (VAO_Pat);
  // pintem el Patricio
  modelTransformPatricio2();
  glDrawArrays(GL_TRIANGLES, 0, patModel.faces().size()*3);
  
  glBindVertexArray (VAO_Pat);
  // pintem el Patricio
  modelTransformPatricio3();
  glDrawArrays(GL_TRIANGLES, 0, patModel.faces().size()*3);
}

void NouGLWidget::resizeGL(int w, int h) {
  MyGLWidget::resizeGL(w, h);
  rav = float(w)/float(h);
  
  if(rav < 1){
      fov = 2*atan(tan(angle)/rav);
      left = -radiEsc;
      right = radiEsc;
      bottom = -radiEsc/rav;
      top = radiEsc/rav;
  }
  else fov = 2*angle;
  if(rav > 1){
      left = -radiEsc*rav;
      right = radiEsc*rav;
      bottom = -radiEsc;
      top = radiEsc;
  }
}

void NouGLWidget::iniCamera ()
{
  MyGLWidget::iniCamera();
  angle = asin(0.5);
  fov = 2*angle;
  glUniform3fv (colFocusLoc, 1, &colFocus[0]);
  left = -radiEsc;
  right = radiEsc;
  bottom = -radiEsc;
  top = radiEsc;
}

void NouGLWidget::iniEscena ()
{
  MyGLWidget::iniEscena();
  centreEsc = glm::vec3(0,1,0);
  radiEsc = distance(glm::vec3(-5,0,-5),centreEsc);
}

void NouGLWidget::modelTransformPatricio ()
{
  //MyGLWidget::modelTransformPatricio();
  // Codi per a la TG necessària
  glm::mat4 patTG = glm::mat4(1.0f);
  patTG = glm::rotate(patTG, angleGir, glm::vec3(0,1,0));
  patTG = glm::translate(patTG, glm::vec3(0,0,-2));
  patTG = glm::rotate(patTG, float(M_PI)/2.0f, glm::vec3(0,1,0));
  patTG = glm::scale(patTG, glm::vec3(2*escalaPat));
  patTG = glm::translate(patTG, -centreBasePat);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &patTG[0][0]);
}


void NouGLWidget::modelTransformPatricio2 ()
{
  // Codi per a la TG necessària
  glm::mat4 patTG = glm::mat4(1.0f);
  patTG = glm::rotate(patTG, float(M_PI)/2.0f + angleGir, glm::vec3(0,1,0));
  patTG = glm::scale(patTG, glm::vec3(2*escalaPat));
  patTG = glm::translate(patTG, -centreBasePat);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &patTG[0][0]);
}

void NouGLWidget::modelTransformPatricio3 ()
{
  // Codi per a la TG necessària
  glm::mat4 patTG = glm::mat4(1.0f);
  patTG = glm::rotate(patTG, angleGir, glm::vec3(0,1,0));
  patTG = glm::translate(patTG, glm::vec3(0,0,2));
  patTG = glm::rotate(patTG, float(M_PI)/2.0f, glm::vec3(0,1,0));
  patTG = glm::scale(patTG, glm::vec3(2*escalaPat));
  patTG = glm::translate(patTG, -centreBasePat);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &patTG[0][0]);
}

void NouGLWidget::viewTransform() {
  //MyGLWidget::viewTransform();

  if(ortogonal){
      glm::vec3 obs = centreEsc + radiEsc*2* glm::vec3(0,1,0); //mirem desde adalt
      View = lookAt(obs, centreEsc, glm::vec3(0,0,-1));
  }
  
  else{
    View = glm::translate(glm::mat4(1.f), glm::vec3(0, 0, -2*radiEsc));
    View = glm::rotate(View, angleX, glm::vec3(1, 0, 0));
    View = glm::rotate(View, -angleY, glm::vec3(0, 1, 0));
    View = glm::translate(View, -centreEsc);
  }
  
  glUniformMatrix4fv (viewLoc, 1, GL_FALSE, &View[0][0]);
  
  glm::vec3 foc;
  foc = glm::vec3(View*glm::vec4(0.0,4.0,0.0,1.0));
  glUniform3fv (posFocusLoc, 1, &foc[0]);
}

void NouGLWidget::projectTransform() {
  //MyGLWidget::projectTransform();
  glm::mat4 Proj;
  if(ortogonal) Proj = glm::ortho(left, right, bottom, top, radiEsc, 3*radiEsc);
  else Proj = glm::perspective(fov, rav, radiEsc, 3*radiEsc);

  glUniformMatrix4fv (projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void NouGLWidget::keyPressEvent(QKeyEvent* event)
{
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_R: {
        angleGir += float(M_PI)/6.0f;
        if(angleGir > float(M_PI)*2.0) angleGir -= float(M_PI)*2;
        emit(enviaGir(float(angleGir*(360.0/(2*float(M_PI))))));
        modelTransformPatricio();
        modelTransformPatricio2();
        modelTransformPatricio3();
      break;
    }
    case Qt::Key_T: {
        if(angleGir < 0.0) angleGir += 2*float(M_PI);
        angleGir -= float(M_PI)/6.0f;
        emit(enviaGir(float(angleGir*(360.0/(2*float(M_PI))))));
        modelTransformPatricio();
        modelTransformPatricio2();
        modelTransformPatricio3();
      break;
    }
    case Qt::Key_C: {
        ortogonal = !ortogonal;
        viewTransform();
        projectTransform();
      break;
    }
    case Qt::Key_O: {
        if(colFocus.x < 1.0){
            colFocus += glm::vec3(0.1,0.1,0.1);
            glUniform3fv (colFocusLoc, 1, &colFocus[0]);
            emit(enviaIntensidad(int(colFocus.x*100.0)));
        }
      break;
    }
    case Qt::Key_L: {
        if(colFocus.x > 0.0){
            colFocus -= glm::vec3(0.1,0.1,0.1);
            glUniform3fv (colFocusLoc, 1, &colFocus[0]);
            emit(enviaIntensidad(int(colFocus.x*100.0)));
        }
      break;
    }
  default: {
      MyGLWidget::keyPressEvent(event);
      break;
    }
  }
  update();
}

void NouGLWidget::setIntensidad(int i){
    makeCurrent();
    colFocus = glm::vec3(float(i)/100.0f);
    glUniform3fv (colFocusLoc, 1, &colFocus[0]);
    update();
}

void NouGLWidget::setGir(int i){
    makeCurrent();
    angleGir = float(i)* ((2.0f*float(M_PI))/360.0); 
    modelTransformPatricio();
    modelTransformPatricio2();
    modelTransformPatricio3();
    update();
}
  
