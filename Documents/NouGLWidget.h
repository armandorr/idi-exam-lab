#include "MyGLWidget.h"

class NouGLWidget : public MyGLWidget
{
 Q_OBJECT
	   
 public:
  NouGLWidget (QWidget *parent=NULL);
  ~NouGLWidget ();

 public slots:
     void setIntensidad(int);
     void setGir(int);

 signals:
     void enviaIntensidad(int);
     void enviaGir(int);

 protected:
  void paintGL ();
  void resizeGL (int w, int h);
  void iniCamera ();
  void iniEscena ();
  void viewTransform ();
  void projectTransform ();
  void modelTransformPatricio ();
  void modelTransformPatricio2 ();
  void modelTransformPatricio3 ();
  
  // keyPressEvent - Es cridat quan es prem una tecla
  void keyPressEvent (QKeyEvent *event);
  
 private:
  float angle;
  float fov;
  
  glm::vec3 colFocus = glm::vec3(1.0,1.0,1.0);
  
  float angleGir = 0.0;
  
  bool ortogonal = false;
  float left, right, bottom, top;
};
