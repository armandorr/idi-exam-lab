#version 330 core

in vec3 vertex; out vec4 vertexSCO;
in vec3 normal; out vec3 normalSCO;

in vec3 matamb; out vec3 matamb1;
in vec3 matdiff; out vec3 matdiff1;
in vec3 matspec; out vec3 matspec1;
in float matshin; out float matshin1;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 TG;

void main()
{	
    vertexSCO = view * TG * vec4(vertex,1.0);
    
    mat3 normalMatrix = inverse(transpose(mat3(view * TG)));
    normalSCO = normalize(normalMatrix * normal);
    
    matamb1 = matamb;
    matdiff1 = matdiff;
    matspec1 = matspec;
    matshin1 = matshin;
    
    gl_Position = proj * view * TG * vec4 (vertex, 1.0);
}
